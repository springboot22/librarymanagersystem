# Library Manager System

## 技术栈

- Spring boot
- maven/gradle 

-  Mybatis

- Redis 

- Spring AOP(@Postconstruct  afterPropertiesSet DisposeBean + ApplicationContext的适用)

- 事务完整性 

-  Java设计模式

-  线程安全 

-  Java8新特性

## 功能点：

1、  管理图书
2、  Role管理
3、  图书借阅
4、  用户管理(用户、角色、group)
5、  日志管理slf4j + log4j

6、  扩展点：用FreeMarker实现不用写代码，但是可以点击页面实现一键生成代码

## 功能设计

## 数据库设计

## 类设计

## 时序图